<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VseeTestController extends Controller
{
    public function patientWaitingRoom()
    {
        return view('patient');
    }

    public function doctorDashboard()
    {
        return view('doctor');
    }
}
