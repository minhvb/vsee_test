/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/doctor.js":
/*!********************************!*\
  !*** ./resources/js/doctor.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

function AppViewModel() {
  var self = this;
  window.pubnub.subscribe({
    channels: [window.pubnub.send_request_channel, window.pubnub.cancel_request_channel],
    withPresence: true
  });
  self.patients = ko.observableArray([]);

  self.addPatient = function (patient) {
    var date = new Date();
    patient.timeCount = date.getTime();
    self.patients.push(patient);
  };

  self.removePatient = function (uuid) {
    var patient = self.patients().find(function (item) {
      return item.uuid === uuid;
    });
    self.patients.remove(patient);
  };

  self.sendMessage = function () {
    window.pubnub.publish({
      channel: window.pubnub.doctor_message_channel,
      message: {
        text: 'A message from doctor'
      }
    }, function (status, response) {
      if (status.error) {
        console.log(status);
      } else {
        console.log('[PUBLISH: sent]', 'timetoken: ' + response.timetoken);
      }
    });
  };

  self.joinCall = function () {
    window.pubnub.publish({
      channel: window.pubnub.doctor_call_channel,
      message: {
        uuid: this.uuid
      }
    }, function (status, response) {
      if (status.error) {
        console.log(status);
      } else {
        console.log('[PUBLISH: sent]', 'timetoken: ' + response.timetoken);
      }
    });
    window.location.href = 'vsee:demo@vsee.com';
  };

  window.pubnub.addListener({
    message: function message(event) {
      switch (event.channel) {
        case window.pubnub.send_request_channel:
          self.addPatient(event.message);
          break;

        case window.pubnub.cancel_request_channel:
          self.removePatient(event.message.uuid);
          break;
      }
    },
    presence: function presence(event) {
      console.log('[PRESENCE: ' + event.action + ']', 'uuid: ' + event.uuid + ', channel: ' + event.channel);
    },
    status: function status(event) {
      console.log('[STATUS: ' + event.category + ']', 'connected to channels: ' + event.affectedChannels);
    }
  });
}

ko.bindingHandlers.timer = {
  update: function update(element) {
    // retrieve the value from the span
    var sec = $(element).text();
    var timer = setInterval(function () {
      var date = new Date();
      var timestamp = date.getTime();
      var minute = Math.floor((parseInt(timestamp) - parseInt(sec)) / 1000 / 60);
      $(element).text(minute);
    }, 1000);
  }
};
ko.applyBindings(new AppViewModel());

/***/ }),

/***/ 2:
/*!**************************************!*\
  !*** multi ./resources/js/doctor.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/chien/working/vsee_test/resources/js/doctor.js */"./resources/js/doctor.js");


/***/ })

/******/ });