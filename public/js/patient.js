/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/patient.js":
/*!*********************************!*\
  !*** ./resources/js/patient.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var Patient = {
  name: ko.observable().extend({
    required: true
  }),
  email: ko.observable().extend({
    required: true,
    pattern: {
      message: 'Email is not valid',
      params: '^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$'
    }
  }),
  reason: ko.observable(),
  vsee_id: ko.observable().extend({
    required: true
  }),
  system_message: ko.observable('Your provider will be with you shortly.'),
  showForm: ko.observable(true),
  showConsultationRoom: ko.observable(false),
  submit: function submit() {
    if (Patient.errors().length !== 0) {
      Patient.errors.showAllMessages();
      return false;
    }

    Patient.showForm(false);
    Patient.showConsultationRoom(true);
    window.pubnub.publish({
      channel: window.pubnub.send_request_channel,
      message: {
        'name': Patient.name(),
        'email': Patient.email(),
        'reason': Patient.reason(),
        'vsee_id': Patient.vsee_id(),
        'uuid': window.pubnub.getUUID()
      }
    }, function (status, response) {
      if (status.error) {
        console.log(status);
      } else {
        console.log('[PUBLISH: sent]', 'timetoken: ' + response.timetoken);
      }
    });
  },
  exitWaitingRoom: function exitWaitingRoom() {
    window.pubnub.publish({
      channel: window.pubnub.cancel_request_channel,
      message: {
        'uuid': window.pubnub.getUUID()
      }
    }, function (status, response) {
      if (status.error) {
        console.log(status);
      } else {
        console.log('[PUBLISH: sent]', 'timetoken: ' + response.timetoken);
      }
    });
    Patient.showForm(true);
    Patient.showConsultationRoom(false);
  }
};
Patient.errors = ko.validation.group(Patient);
ko.applyBindings(Patient);
window.pubnub.subscribe({
  channels: [window.pubnub.doctor_message_channel, window.pubnub.doctor_call_channel],
  withPresence: true
});
window.pubnub.addListener({
  message: function message(event) {
    switch (event.channel) {
      case window.pubnub.doctor_message_channel:
        Patient.system_message(event.message.text);
        break;

      case window.pubnub.doctor_call_channel:
        if (window.pubnub.getUUID() === event.message.uuid) {
          Patient.system_message('The visit is in progress');
        } else {
          Patient.system_message('Doctor is currently busy and will attend to you soon');
        }

        break;

      default:
        break;
    }
  },
  presence: function presence(event) {
    console.log('[PRESENCE: ' + event.action + ']', 'uuid: ' + event.uuid + ', channel: ' + event.channel);
  },
  status: function status(event) {
    console.log('[STATUS: ' + event.category + ']', 'connected to channels: ' + event.affectedChannels);
  }
});

/***/ }),

/***/ 1:
/*!***************************************!*\
  !*** multi ./resources/js/patient.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/chien/working/vsee_test/resources/js/patient.js */"./resources/js/patient.js");


/***/ })

/******/ });