## Building the test

```
$ cp .env.example .env
$ cd docker
$ docker-compose up -d
$ docker-compose exec workspace composer install
$ docker-compose exec workspace php artisan key:generate
$ cd ..
$ npm install 
$ npm run development
```

Javascript files was located in `resources/js` and can be compile with Webpack
## Running the test
Go to  [http://localhost:8888](http://localhost:8888) to try the test
