@extends('layouts.main')

@section('title', 'Page Title')

@section('content')
    <div class="d-flex justify-content-center">
        <div class="col-11">
            <div class="card mt-5">
                <div class="card-header bg-success">
                    <a data-toggle="collapse" href="#test-block" aria-expanded="true" aria-controls="test-block" class="">
                        <h5 class="mb-0 text-white font-weight-bold"><i class="fas fa-align-justify"></i> Waiting room</h5>
                    </a>
                </div>
                <div id="test-block" class="collapse show">
                    <div class="card-body">
                        <ul class="list-unstyled" data-bind="foreach: patients">
                            <li class="media">
                                <h1 class="col-md-1"><i class="fas fa-user"></i></h1>
                                <div class="col-md-6 pl-0">
                                    <h6 class="mt-0 mb-1 font-weight-bold" data-bind="text: name"></h6>
                                    <p data-bind="text: reason"></p>
                                    <p data-bind="text: email"></p>
                                    <p><span>Vsee ID: </span><span data-bind="text: vsee_id"></span></p>
                                </div>
                                <div class="col-md-3">
                                    <p><i class="fas fa-video text-success"></i> Online</p>
                                    <p><i class="far fa-clock"></i> Waiting: <span data-bind="text: timeCount, timer: timeCount"></span> min</p>
                                </div>
                                <div class="col-md-3">
                                    <button data-bind="click: $parent.sendMessage" type="button" class="btn btn-warning"><i class="fas fa-comment text-white"></i></button>
                                    <button data-bind="click: $parent.joinCall" class="btn btn-warning"><i class="fas fa-video text-white"></i></button>
                                    <button type="button" class="btn btn-warning"><i class="fas fa-ellipsis-h text-white"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
    <script src="{{ asset('js/doctor.js') }}"></script>
@endsection
