@extends('layouts.main')

@section('title', 'Page Title')

@section('content')
    <div class="d-flex justify-content-center">
        <div class="col-8">
            <h3 class="content-title mt-5 text-center">
                <strong>Welcome to Code Challenge Waiting Room</strong>
            </h3>
            <p class="text-center">If it is an emergency, please call 911.</p>
            <div class="card" data-bind="visible: showForm">
                <div class="card-header bg-success">
                    <h5 class="mb-0 text-white font-weight-bold"><i class="fas fa-video"></i> Talk to An Nguyen</h5>
                </div>
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <label for="input-name" class="font-weight-bold">Please fill in your name to proceed<span
                                    class="text-danger">*</span></label>
                            <input data-bind="value: name" type="text" class="form-control" placeholder="Your name"
                                   id="input-name" aria-describedby="name">
                        </div>
                        <div class="form-group">
                            <label for="input-email" class="font-weight-bold">Please fill in your email to proceed<span
                                    class="text-danger">*</span></label>
                            <input data-bind="value: email" type="email" class="form-control" placeholder="Your email"
                                   id="input-email" aria-describedby="email">
                        </div>
                        <div class="form-group">
                            <label for="reason"><span class="font-weight-bold">Reason for visit</span>
                                (optional)</label>
                            <textarea data-bind="value: reason" name="reason" placeholder="Your reason to visit"
                                      id="reason" cols="30" rows="3" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="input-vsee-id" class="font-weight-bold">Vsee ID<span
                                    class="text-danger">*</span></label>
                            <input data-bind="value: vsee_id" type="text" class="form-control"
                                   placeholder="Your Vsee ID for doctor to call" id="input-vsee-id"
                                   aria-describedby="vsee-id">
                        </div>
                        <button type="button" class="btn btn-warning" data-bind='click: submit'><span
                                class="font-weight-bold text-white">Enter waiting room</span></button>
                    </form>
                </div>
            </div>
            <div class="card" data-bind="visible: showConsultationRoom">
                <div class="card-header bg-success">
                    <h5 class="mb-0 text-white font-weight-bold"><i class="far fa-clock"></i> Connecting you with provider</h5>
                </div>
                <div class="card-body">
                    <h5 class="font-weight-bold text-center mt-5 mb-3" data-bind="text: system_message"></h5>
                    <div class="d-flex justify-content-center mb-5">
                        <button data-bind="click: exitWaitingRoom" class="btn btn-warning text-white font-weight-bold">Exit waiting room</button>
                    </div>
                </div>
                <div class="card-footer bg-white">
                    <p class="text-center">If you close the video conference by mistake please, <span class="text-success">click here to relaunch</span> video again</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
    <script src="{{ asset('js/patient.js') }}"></script>
@endsection
