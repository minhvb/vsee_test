function AppViewModel() {
    var self = this;

    window.pubnub.subscribe({
        channels: [
            window.pubnub.send_request_channel,
            window.pubnub.cancel_request_channel
        ],
        withPresence: true
    });

    self.patients = ko.observableArray([]);

    self.addPatient = function(patient) {
        var date = new Date();
        patient.timeCount = date.getTime();
        self.patients.push(patient);
    };

    self.removePatient = function(uuid) {
        let patient = self.patients().find(function (item) {
            return item.uuid === uuid
        });

        self.patients.remove(patient);
    };

    self.sendMessage = function() {
        window.pubnub.publish({
                channel : window.pubnub.doctor_message_channel,
                message : {
                    text: 'A message from doctor'
                }
            },
            function(status, response) {
                if (status.error) {
                    console.log(status)
                }
                else {
                    console.log('[PUBLISH: sent]',
                        'timetoken: ' + response.timetoken);
                }
            });
    };

    self.joinCall = function() {
        window.pubnub.publish({
                channel : window.pubnub.doctor_call_channel,
                message : {
                    uuid: this.uuid
                }
            },
            function(status, response) {
                if (status.error) {
                    console.log(status)
                }
                else {
                    console.log('[PUBLISH: sent]',
                        'timetoken: ' + response.timetoken);
                }
            });

        window.location.href = 'vsee:demo@vsee.com';
    };

    window.pubnub.addListener({
        message: function(event) {
            switch (event.channel) {
                case window.pubnub.send_request_channel:
                    self.addPatient(event.message);
                    break;
                case window.pubnub.cancel_request_channel:
                    self.removePatient(event.message.uuid);
                    break;
            }
        },
        presence: function(event) {
            console.log('[PRESENCE: ' + event.action + ']',
                'uuid: ' + event.uuid + ', channel: ' + event.channel);
        },
        status: function(event) {
            console.log('[STATUS: ' + event.category + ']',
                'connected to channels: ' + event.affectedChannels);
        }
    });

}
ko.bindingHandlers.timer = {
    update: function (element) {
        // retrieve the value from the span
        let sec = $(element).text();
        let timer = setInterval(function() {
            let date = new Date();
            let timestamp = date.getTime();
            let minute = Math.floor(((parseInt(timestamp) - parseInt(sec)) / 1000) / 60);
            $(element).text(minute);
        }, 1000);

    }
};
ko.applyBindings(new AppViewModel());
