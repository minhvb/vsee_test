window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });

const uuid = PubNub.generateUUID();
window.pubnub = new PubNub({
    // replace the key placeholders with your own PubNub publish and subscribe keys
    publishKey: 'pub-c-f10f10b9-ae13-4413-8b86-7108c2a9fc0b',
    subscribeKey: 'sub-c-2c160c50-c1bd-11ea-a44f-6e05387a1df4',
    uuid: uuid
});

window.pubnub.send_request_channel = 'vsee_test_send_request';
window.pubnub.cancel_request_channel = 'vsee_test_cancel_request';
window.pubnub.doctor_message_channel = 'vsee_test_doctor_message';
window.pubnub.doctor_call_channel = 'vsee_test_doctor_call';
