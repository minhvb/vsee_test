var Patient = {
    name: ko.observable().extend({ required: true }),
    email: ko.observable().extend({
        required: true,
        pattern: {
            message: 'Email is not valid',
            params: '^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$'
        }
    }),
    reason: ko.observable(),
    vsee_id: ko.observable().extend({ required: true }),
    system_message: ko.observable('Your provider will be with you shortly.'),
    showForm: ko.observable(true),
    showConsultationRoom: ko.observable(false),

    submit: function() {
        if (Patient.errors().length !== 0) {
            Patient.errors.showAllMessages();
            return false;
        }

        Patient.showForm(false);
        Patient.showConsultationRoom(true);

        window.pubnub.publish({
            channel : window.pubnub.send_request_channel,
                message : {
                    'name' : Patient.name(),
                    'email' : Patient.email(),
                    'reason' : Patient.reason(),
                    'vsee_id' : Patient.vsee_id(),
                    'uuid': window.pubnub.getUUID()
                }
            },
            function(status, response) {
                if (status.error) {
                    console.log(status)
                }
                else {
                    console.log('[PUBLISH: sent]',
                        'timetoken: ' + response.timetoken);
                }
            });
    },

    exitWaitingRoom : function() {
        window.pubnub.publish({
                channel : window.pubnub.cancel_request_channel,
                message : {
                    'uuid': window.pubnub.getUUID()
                }
            },
            function(status, response) {
                if (status.error) {
                    console.log(status)
                }
                else {
                    console.log('[PUBLISH: sent]',
                        'timetoken: ' + response.timetoken);
                }
            });

        Patient.showForm(true);
        Patient.showConsultationRoom(false);
    }
};

Patient.errors = ko.validation.group(Patient);
ko.applyBindings(Patient);


window.pubnub.subscribe({
    channels: [
        window.pubnub.doctor_message_channel,
        window.pubnub.doctor_call_channel
    ],
    withPresence: true
});

window.pubnub.addListener({
    message: function(event) {
        switch (event.channel) {
            case window.pubnub.doctor_message_channel:
                Patient.system_message(event.message.text);
                break;
            case window.pubnub.doctor_call_channel:
                if (window.pubnub.getUUID() === event.message.uuid) {
                    Patient.system_message('The visit is in progress');
                } else {
                    Patient.system_message('Doctor is currently busy and will attend to you soon');
                }
                break;
            default:
                break;
        }
    },
    presence: function(event) {
        console.log('[PRESENCE: ' + event.action + ']',
            'uuid: ' + event.uuid + ', channel: ' + event.channel);
    },
    status: function(event) {
        console.log('[STATUS: ' + event.category + ']',
            'connected to channels: ' + event.affectedChannels);
    }
});
